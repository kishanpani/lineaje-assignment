package main

import (
	"fmt"
	"lineaje-assignment/pkg/dependencyGraph"

	"github.com/KyleBanks/depth"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
)

const (
	repoURL    = "https://github.com/etcd-io/etcd"
	branchName = "main"
)

func main() {
	_, err := git.PlainClone("./data/etcd", false, &git.CloneOptions{
		URL:           repoURL,
		ReferenceName: plumbing.ReferenceName(branchName),
	})

	if err != nil {
		fmt.Println("Download failed")
		return
	} else {
		fmt.Println("Download successful")
	}

	var t depth.Tree
	t.Resolve("./data/etcd")
	dependencyGraph.CreateDependencyGraph(*t.Root)
}
