package dependencyGraph

import (
	"encoding/json"
	"os"

	"github.com/KyleBanks/depth"
)

func CreateDependencyGraph(root depth.Pkg) error {
	artifactTree := populateDependencyTree(root)
	createJSON(artifactTree)
	return nil
}

func populateDependencyTree(rawDep depth.Pkg) Artifact {
	var currentArtifact Artifact = Artifact{
		Name:         rawDep.Name,
		Dependencies: []*Artifact{},
	}
	if len(rawDep.Deps) != 0 {
		for _, dep := range rawDep.Deps {
			nextArtifact := populateDependencyTree(dep)
			tempDependencies := currentArtifact.Dependencies
			tempDependencies = append(tempDependencies, &nextArtifact)
			currentArtifact.Dependencies = tempDependencies
		}
	}
	return currentArtifact
}

func createJSON(artifact Artifact) error {
	file, err := os.OpenFile("./data/artifacts.json", os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		return err
	}
	defer file.Close()
	artifactByte, err := json.Marshal(artifact)
	if err != nil {
		return err
	}
	_, err = file.Write(artifactByte)
	if err != nil {
		return err
	}
	return nil
}
